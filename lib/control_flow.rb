# EASY

# Return the argument with all its lowercase characters removed.
def destructive_uppercase(str)
  lower_case = ("a".."z").to_a.join
  str.delete!(lower_case)
end

# Return the middle character of a string. Return the middle two characters if
# the word is of even length, e.g. middle_substring("middle") => "dd",
# middle_substring("mid") => "i"
def middle_substring(str)
  str_midpoint = str.length / 2
  if str.length.odd?
    return str[str_midpoint]
  else
    return str[(str_midpoint-1)..str_midpoint]
  end
end

# Return the number of vowels in a string.
VOWELS = %w(a e i o u)

def is_vowel?(ch)
  vowels = "aeiou"
  vowels.include?(ch.downcase)
end

def num_vowels(str)
  str.chars.count { |ch| is_vowel?(ch) }
end

# Return the factoral of the argument (num). A number's factorial is the product
# of all whole numbers between 1 and the number itself. Assume the argument will
# be > 0.
def factorial(num)
  (1..num).to_a.reduce(:*)
end


# MEDIUM

# Write your own version of the join method. separator = "" ensures that the
# default seperator is an empty string.
def my_join(arr, separator = "")
  target_str = ""

  arr.each_index do |idx|
    target_str << arr[idx]
    target_str << separator unless idx == arr.length - 1
  end

  target_str
end

# Write a method that converts its argument to weirdcase, where every odd
# character is lowercase and every even is uppercase, e.g.
# weirdcase("weirdcase") => "wEiRdCaSe"
def weirdcase(str)
  target_str = ""

  str.chars.each_index do |idx|
    if (idx + 1).odd?
      target_str << str[idx].downcase
    else
      target_str << str[idx].upcase
    end
  end

  target_str
end

# Reverse all words of five more more letters in a string. Return the resulting
# string, e.g., reverse_five("Looks like my luck has reversed") => "skooL like
# my luck has desrever")
def reverse_five(str)
  target_arr = []

  str.split.each do |word|
    if word.length >= 5
      target_arr << word.reverse
    else
      target_arr << word
    end
  end

  target_arr.join(" ")
end

# Return an array of integers from 1 to n (inclusive), except for each multiple
# of 3 replace the integer with "fizz", for each multiple of 5 replace the
# integer with "buzz", and for each multiple of both 3 and 5, replace the
# integer with "fizzbuzz".
def fizzbuzz(n)
  fizzbuzz_arr = []

  (1..n).each do |i|
    if i % 15 == 0
      fizzbuzz_arr << "fizzbuzz"
    elsif i % 3 == 0
      fizzbuzz_arr << "fizz"
    elsif i % 5 == 0
      fizzbuzz_arr << "buzz"
    else
      fizzbuzz_arr << i
    end
  end

  fizzbuzz_arr
end


# HARD

# Write a method that returns a new array containing all the elements of the
# original array in reverse order.
def my_reverse(arr)
  arr.reverse
end

# Write a method that returns a boolean indicating whether the argument is
# prime.
def prime?(num)
  return false if num <= 1

  (2...num).each do |i|
    return false if num % i == 0
  end

  true
end

# Write a method that returns a sorted array of the factors of its argument.
def factors(num)
  factor_arr = []

  for i in (1..num)
    factor_arr << i if num % i == 0
  end

  factor_arr.sort
end

# Write a method that returns a sorted array of the prime factors of its argument.
def prime_factors(num)
  prime_factors = []

  factors(num).each do |factor|
    prime_factors << factor if prime?(factor)
  end

  prime_factors.sort
end

# Write a method that returns the number of prime factors of its argument.
def num_prime_factors(num)
  prime_factors(num).count
end


# EXPERT

# Return the one integer in an array that is even or odd while the rest are of
# opposite parity, e.g. oddball([1,2,3]) => 2, oddball([2,4,5,6] => 5)
def oddball(arr)
  odd_count = arr.count { |i| i.odd? }
  even_count = arr.count { |i| i.even? }

  if odd_count > even_count
    return arr.select { |i| i.even? }[0]
  else
    return arr.select { |i| i.odd? }[0]
  end
end
